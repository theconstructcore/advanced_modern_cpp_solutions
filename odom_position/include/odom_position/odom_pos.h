#ifndef TEST_LIBRARY_H
#define TEST_LIBRARY_H

#include "nav_msgs/Odometry.h"
#include <ros/ros.h>

using namespace std;

class Odom_Data {
public:
  Odom_Data(ros::NodeHandle &ros_node);
  ~Odom_Data();
  void odomCallback(const nav_msgs::Odometry::ConstPtr &msg);

  ros::NodeHandle *ros_node_odom_class;
  // Odometry data
  ros::Subscriber odom_sub;
  float x_pos;
  float y_pos;
  float z_pos;
};

#endif