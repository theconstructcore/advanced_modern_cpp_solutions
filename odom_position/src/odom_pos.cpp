#include "odom_position/odom_pos.h"

Odom_Data::Odom_Data(ros::NodeHandle &ros_node) {

  this->ros_node_odom_class = &ros_node;

  if (ros::console::set_logger_level(ROSCONSOLE_DEFAULT_NAME,
                                     ros::console::levels::Debug)) {
    ros::console::notifyLoggerLevelsChanged();
  }

  odom_sub = this->ros_node_odom_class->subscribe(
      "/robot1/odom", 10, &Odom_Data::odomCallback, this);

  x_pos = 0.0;
  y_pos = 0.0;
  z_pos = 0.0;
}

void Odom_Data::odomCallback(const nav_msgs::Odometry::ConstPtr &msg) {

  this->x_pos = msg->pose.pose.position.x;
  this->y_pos = msg->pose.pose.position.y;
  this->z_pos = msg->pose.pose.position.x;

  ROS_INFO_STREAM("Sending random velocity command: "
                  << " X=" << this->x_pos << "Y=" << this->y_pos
                  << "Z=" << this->z_pos);
}

// Destructor
Odom_Data::~Odom_Data() { cout << "Destructor is called" << endl; }