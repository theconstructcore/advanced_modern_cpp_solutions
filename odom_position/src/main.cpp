#include "odom_position/odom_pos.h"

int main(int argc, char **argv) {

  ros::init(argc, argv, "odom_position");

  ros::NodeHandle _n("odom_namespace");

  Odom_Data odom_data_obj(_n);

  ros::spin();

  return 0;
}