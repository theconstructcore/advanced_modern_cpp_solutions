template <class T> class X {
public:
  template <class U> class Y {
  public:
    template <class V> void f(U, V);
    void g(U);
  };
};

template <class T>
template <class U>
template <class V>
void X<T>::Y<U>::f(U, V) {
  cout << "Template 1" << endl;
}

template <class T> template <class U> void X<T>::Y<U>::g(U) {
  cout << "Template 2" << endl;
}

template <> template <> void X<int>::Y<int>::g(int) {
  cout << "Template 3" << endl;
}

template <> template <> template <class V> void X<int>::Y<int>::f(int, V) {
  cout << "Template 4" << endl;
}

template <> template <> template <> void X<int>::Y<int>::f<int>(int, int) {
  cout << "Template 5" << endl;
}

// template<> template<class U> template<class V>
//    void X<char>::Y<U>::f(U, V) { cout << "Template 6" << endl; }

// template<class T> template<>
//    void X<T>::Y<float>::g(float) { cout << "Template 7" << endl; }

int main() {
  MagicSubscriber<sensor_msgs::Image> magic_subscriber_object(_n, topic_name);
  X<int>::Y<int> a;
  X<char>::Y<char> b;
  a.f(1, 2);
  a.f(3, 'x');
  a.g(3);
  b.f('x', 'y');
  b.g('z');
}