#ifndef MAGIC_SUBSCRIBER_H
#define MAGIC_SUBSCRIBER_H
#include <ros/ros.h>

template <class MyClassTemplateArg> class MyClass {
public:
  void PrintInputType(std::string in_arg) {
    std::cout << "Print Type" << typeid(in_arg).name() << std::endl;
  }
};

template <> class MyClass<std::string> {
public:
  void PrintInputString(std::string in_arg) {
    std::cout << "Message=" << in_arg << std::endl;
  }
};
#endif