#include "unit6_exercises/auxlib_basic_example_u6_specialization.h"

int main(int argc, char **argv) {
  ros::init(argc, argv, "basic_example_u6_specialization_node");
  ros::NodeHandle _n("basic_example_u6_specialization_ns");

  MyClass<std::string> object_1;
  object_1.PrintInputString("Adventure Time!");

  MyClass<int> object_2;
  object_2.PrintInputType("Adventure Time!");

  return 0;
}
